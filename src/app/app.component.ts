import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  template: `
    <h1>Hello IC</h1>
    <div class="container">
      <router-outlet></router-outlet>
    </div>
  `,
  styles: []
})
export class AppComponent {
}
