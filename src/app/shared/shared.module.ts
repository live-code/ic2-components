import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CardComponent } from './components/cards/card.component';
import { CardListComponent } from './components/cards/variants/card-list.component';
import { CardFacebookComponent } from './components/cards/variants/card-facebook.component';
import { TabbarComponent } from './components/tabbar.component';



@NgModule({
  declarations: [
    CardComponent,
    CardListComponent,
    CardFacebookComponent,
    TabbarComponent,
  ],
  imports: [
    CommonModule
  ],
  exports: [
    CardComponent,
    CardListComponent,
    CardFacebookComponent,
    TabbarComponent
  ]
})
export class SharedModule { }
