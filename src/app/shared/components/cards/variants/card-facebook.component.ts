import { Component } from '@angular/core';

@Component({
  selector: 'app-card-facebook',
  template: `
    <app-card
      title="Facebook"
      icon="fa fa-facebook"
      headerCls="bg-info text-white"
      (iconClick)="openUrl('http//www.google.com')"
    >
      <ng-content></ng-content>

    </app-card>

    
  `,
  styles: [
  ]
})
export class CardFacebookComponent {

  openUrl(url: string) {
    window.open(url)
  }
}
