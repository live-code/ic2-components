import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-card-list',
  template: `

    <app-card
      headerCls="bg-dark text-white"
      icon="fa fa-facebook"
      (iconClick)="alert('bla bla')"
      title="LIST"
      [centered]="true"
    >
      <li *ngFor="let u of data" (click)="alert(u)">{{u}}</li>
    </app-card>

  `,
  styles: [
  ]
})
export class CardListComponent {
  @Input() data: any[]  = []


  alert(data: string) {
    window.alert(data)
  }
}
