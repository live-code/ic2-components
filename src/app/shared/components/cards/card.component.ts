import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-card',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <div class="card">
      <div 
        class="card-header"
        [ngClass]="headerCls"
        *ngIf="title"
      >
        <div class="d-flex justify-content-between align-items-center">
          {{title}}
          <i 
            *ngIf="icon" 
            [class]="icon"
            (click)="iconClick.emit()"
          ></i>
        </div>
      </div>
      <div 
        class="card-body"
        [ngClass]="{'text-center': centered}"
      >
        <ng-content></ng-content>
      </div>
    </div>
    {{render()}}
  `,
})
export class CardComponent {
  @Input() title: string | undefined = '';
  @Input() headerCls: string = ''
  @Input() icon: string = ''
  @Input() centered = false;
  @Output() iconClick = new EventEmitter()

  doSomething() {
    window.alert('pippo')
  }

  render() {
    console.log('render card')
  }
}
