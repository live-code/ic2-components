import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Country } from '../../features/uikit2/uikit2.component';

@Component({
  selector: 'app-tabbar',
  template: `
    <ul class="nav nav-tabs">
      <li 
        class="nav-item"
        *ngFor="let item of items"
        (click)="tabClick.emit(item)"
      >
        <a 
          class="nav-link"
          [ngClass]="{'active': item.id === active?.id}"
        >
          {{item[labelField]}}
          <i [class]="icon" (click)="iconCLickHandler($event, item)"></i>
        </a>
      </li>
    </ul>
  `,
})
export class TabbarComponent<T extends {id: number, [key: string]: any}> {
  @Input() items: T[] = [];
  @Input() active: T | undefined;
  @Input() icon: string | undefined;
  @Input() labelField: string = 'label'
  @Output() tabClick = new EventEmitter<T>();
  @Output() iconTabClick = new EventEmitter<T>();

  iconCLickHandler(event: MouseEvent, item: T) {
    event.stopPropagation();
    this.iconTabClick.emit(item)
  }
}

