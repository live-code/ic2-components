import { Component } from '@angular/core';

export interface Country {
  id: number;
  label: string;
  desc: string;
  cities: City[]
}
export interface City {
  id: number;
  name: string;
  desc: string
}

@Component({
  selector: 'app-uikit2',
  template: `
    <h1>Tabbar</h1>
    <app-tabbar
      [items]="countries"
      [active]="activeCountry"
      icon="fa fa-times"
      (iconTabClick)="removeTab($event)"
      (tabClick)="countryCLickHandler($event)"
    ></app-tabbar>
    
    <app-tabbar
      *ngIf="activeCountry"
      labelField="name"
      icon="fa fa-link"
      (iconTabClick)="openWiki($event)"
      [items]="activeCountry.cities"
      [active]="activeCity"
      (tabClick)="cityClickHandler($event)"
    ></app-tabbar>
    
    <div *ngIf="activeCity">
      {{activeCity.desc}}
    </div>
  `,
})
export class Uikit2Component {
  countries: Country[] = []
  activeCountry: Country | undefined;
  activeCity: City | undefined;

  constructor() {
      this.countries = [
        {
          id: 2,
          label: 'germany',
          desc: 'bla bla 2',
          cities: [
            { id: 1, name: 'Berlin', desc: 'city 1 description....' },
            { id: 2, name: 'Monaco', desc: 'city 2 description....' }
          ]
        },
        {
          id: 1, label: 'italy', desc: 'bla bla 1',
          cities: [
            { id: 11, name: 'Rome', desc: 'city 3 description....' },
            { id: 22, name: 'Milan', desc: 'city 4 description....' },
            { id: 33, name: 'Palermo', desc: 'city 5 description....' },
          ]
        },
        { id: 3, label: 'spain', desc: 'bla bla 3',
          cities: [
            {id: 41, name: 'Madrid', desc: 'city 6description....'}
          ]},
      ];
      this.countryCLickHandler(this.countries[0])
  }
  countryCLickHandler(item: Country) {
    this.activeCountry = item;
    this.activeCity = this.activeCountry.cities[0]
  }

  cityClickHandler(city: City) {
    this.activeCity = city;
  }

  openWiki(city: City) {
    window.open(`https://en.wikipedia.org/wiki/${city.name}`)
  }

  removeTab(country: Country) {
    this.countries =
      this.countries.filter(c => c.id !== country.id)
  }
}
