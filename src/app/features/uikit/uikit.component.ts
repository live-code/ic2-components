import { Component, TemplateRef, ViewContainerRef } from '@angular/core';

@Component({
  selector: 'app-uikit',
  template: `
    <app-card
      title="Google" 
      icon="fa fa-google"
      headerCls="bg-info text-white"
      (iconClick)="openUrl('http//www.google.com')"
    >
      ciao
    </app-card>

    <app-card-facebook>
      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Asperiores delectus ea earum exercitationem fugiat, minus neque nihil perferendis quibusdam, quo quos repellendus vero? Architecto ipsa obcaecati odit quis sit vel.
    </app-card-facebook>
    
    <app-card-list [data]="users"></app-card-list>
    <app-card-list [data]="[1, 2, 3]"></app-card-list>
   
  `,
})
export class UikitComponent {
  users = ['one', 'two', 'three']

  openUrl(url: string) {
    window.open(url)
  }
}

