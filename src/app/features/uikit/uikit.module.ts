import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { CardComponent } from '../../shared/components/cards/card.component';
import { SharedModule } from '../../shared/shared.module';

import { UikitRoutingModule } from './uikit-routing.module';
import { UikitComponent } from './uikit.component';


@NgModule({
  declarations: [
    UikitComponent,
  ],
  imports: [
    CommonModule,
    UikitRoutingModule,
    SharedModule,
    FormsModule
  ]
})
export class UikitModule { }
